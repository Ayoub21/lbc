package com.boukharist.leboncoin.utils

interface Instruction {
    fun checkCondition(): Boolean
    fun getDescription(): String
}