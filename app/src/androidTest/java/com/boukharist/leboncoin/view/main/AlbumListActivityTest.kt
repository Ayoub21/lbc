package com.boukharist.leboncoin.view.main


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.withClassName
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.utils.ConditionWatcher
import com.boukharist.leboncoin.utils.Instruction
import com.boukharist.leboncoin.utils.ListUtils
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AlbumListActivityTest {


    @get:Rule
    var activityTestRule = ActivityTestRule(AlbumListActivity::class.java)

    @Test
    fun albumListActivityTest() {
        val albumRecyclerView = onView(allOf(withId(R.id.recycler), childAtPosition(withClassName(`is`("android.widget.LinearLayout")), 1)))

        //Wait for pictures  list to load
        ConditionWatcher.waitForCondition(object : Instruction {
            override fun getDescription(): String {
                return "wait for albums list to load"
            }

            override fun checkCondition(): Boolean {
                return ListUtils.exists(albumRecyclerView)
            }
        })

        //click the first item
        albumRecyclerView.perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))


        //get the pictures recycler
        val picturesRecyclerView = onView(
                allOf(withId(R.id.recycler),
                        childAtPosition(withClassName(`is`("android.widget.LinearLayout")), 1)))

        //Wait for pictures  list to load
        ConditionWatcher.waitForCondition(object : Instruction {
            override fun getDescription(): String {
                return "wait for pictures list to load"
            }

            override fun checkCondition(): Boolean {
                return ListUtils.exists(picturesRecyclerView)
            }
        })

        picturesRecyclerView.perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        //try to swipe some times
        onView(withId(R.id.view_pager)).perform(swipeRight())
        onView(withId(R.id.view_pager)).perform(swipeLeft())
    }

    private fun childAtPosition(parentMatcher: Matcher<View>, position: Int): Matcher<View> {
        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }
}
