package com.boukharist.leboncoin.view.main

import android.widget.ImageView
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.data.datasource.local.Album
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class AlbumsAdapter : BaseQuickAdapter<Album, BaseViewHolder>(R.layout.list_item_album) {

    override fun convert(helper: BaseViewHolder?, item: Album) {

        helper?.setText(R.id.title, mContext.getString(R.string.album_title, item.id))
        helper?.setText(R.id.pic_count, mContext.getString(R.string.album_pics_count, item.picturesCount))

        Glide.with(mContext)
                .load(item.thumbnail)
                .into(helper?.getView(R.id.thumbnail) as ImageView)
    }
}