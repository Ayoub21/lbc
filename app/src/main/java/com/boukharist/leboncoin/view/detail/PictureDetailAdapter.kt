package com.boukharist.leboncoin.view.detail

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.utils.GlideLoadingImageListener
import com.bumptech.glide.Glide


class PictureDetailAdapter(private val pics: List<PicEntity>) : PagerAdapter() {
    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var scaleFactor = 1.0f
    /*
    ************************************************************************************************
    ** Life cycle
    ************************************************************************************************
     */
    override fun getCount(): Int {
        return pics.size
    }

    override fun isViewFromObject(view: View, key: Any): Boolean {
        return view === key
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        //Get the "page" linked to the wanted position
        val currentPic = pics[position]

        //Inflate the view
        val pageContent = LayoutInflater
                .from(container.context)
                .inflate(R.layout.item_picture_detail, container, false)

        //Setup picture
        val imageView = pageContent.findViewById<ImageView>(R.id.image_view)
        val loadingView = pageContent.findViewById<ImageView>(R.id.loading_view)
        val errorView = pageContent.findViewById<ImageView>(R.id.error_view)
        Glide.with(container.context)
                .load(currentPic.url)
                .listener(GlideLoadingImageListener(loadingView, errorView))
                .into(imageView)

        //setup title
        val titleTextView = pageContent.findViewById<TextView>(R.id.title)
        titleTextView.text = currentPic.title

        //Add newly created view in the given container
        container.addView(pageContent)

        //Return the newly created view
        return pageContent
    }

    override fun destroyItem(container: ViewGroup, position: Int, key: Any) {
        container.removeView(key as View)
    }


}