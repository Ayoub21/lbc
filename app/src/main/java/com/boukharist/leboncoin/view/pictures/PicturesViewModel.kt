package com.boukharist.leboncoin.view.pictures

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.boukharist.leboncoin.data.datasource.local.Album
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.data.repository.PicsRepository
import com.boukharist.leboncoin.utils.SchedulerProvider
import com.boukharist.leboncoin.utils.getTag
import com.boukharist.leboncoin.view.base.DisposableViewModel
import com.boukharist.leboncoin.view.base.ErrorState
import com.boukharist.leboncoin.view.base.LoadingState
import com.boukharist.leboncoin.view.base.ViewModelState

@SuppressLint("RxLeakedSubscription")
class PicturesViewModel(private val repository: PicsRepository, private val schedulerProvider: SchedulerProvider) : DisposableViewModel() {


    private val _states = MutableLiveData<ViewModelState>()
    val states: LiveData<ViewModelState>
        get() = _states


    fun getPicturesOf(albumId: Int) {
        launch {
            repository.getAllPics(albumId)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { _states.postValue(LoadingState) }
                    .subscribe({ list ->
                        _states.postValue(LoadedState(list))
                    }, { throwable ->
                        _states.postValue(ErrorState(throwable))
                        Log.e(getTag(), throwable.message, throwable)
                    })
        }
    }

    data class LoadedState(val value: List<PicEntity>) : ViewModelState()
}