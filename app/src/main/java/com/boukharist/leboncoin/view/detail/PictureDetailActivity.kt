package com.boukharist.leboncoin.view.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.utils.extra
import kotlinx.android.synthetic.main.activity_picture_detail.*

class PictureDetailActivity : AppCompatActivity() {

    /*
     ************************************************************************************************
     ** Fields
     ************************************************************************************************
     */
    private val position by extra<Int>(EXTRA_POSITION)
    private val pics by extra<List<PicEntity>>(EXTRA_PIC_LIST)

    /*
     ************************************************************************************************
     ** LifeCycle Methods
     ************************************************************************************************
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture_detail)
        setupViewPager()
    }

    private fun setupViewPager() {
        view_pager.apply {
            adapter = PictureDetailAdapter(pics)
            post { setCurrentItem(position, false) }
        }
    }

    companion object {
        private const val EXTRA_POSITION = "EXTRA_POSITION"
        private const val EXTRA_PIC_LIST = "EXTRA_PIC_LIST"
        fun newIntent(context: Context, position: Int, pics: List<PicEntity>): Intent {
            return Intent(context, PictureDetailActivity::class.java).apply {
                putExtra(EXTRA_POSITION, position)
                putParcelableArrayListExtra(EXTRA_PIC_LIST, ArrayList(pics))
            }
        }
    }
}
