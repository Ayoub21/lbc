package com.boukharist.leboncoin.view.pictures

import android.widget.ImageView
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.utils.GlideLoadingImageListener
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder

class PicturesAdapter : BaseQuickAdapter<PicEntity, BaseViewHolder>(R.layout.list_item_picture) {

    override fun convert(helper: BaseViewHolder?, item: PicEntity) {

        helper?.let {
            val loadingView = it.getView<ImageView>(R.id.loading_view)
            val errorView = it.getView<ImageView>(R.id.error_view)
            val imageView = it.getView<ImageView>(R.id.thumbnail)

            Glide.with(mContext)
                    .load(item.thumbnail)
                    .listener(GlideLoadingImageListener(loadingView,errorView))
                    .into(imageView)
        }

    }
}