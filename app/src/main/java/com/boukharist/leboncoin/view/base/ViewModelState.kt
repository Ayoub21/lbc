package com.boukharist.leboncoin.view.base

open class ViewModelState

/**
 * Generic Loading ViewModel State
 */
object LoadingState : ViewModelState()


data class ErrorState(val error: Throwable) : ViewModelState()