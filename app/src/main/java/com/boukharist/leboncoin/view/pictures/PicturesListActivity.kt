package com.boukharist.leboncoin.view.pictures

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.boukharist.leboncoin.R
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.utils.NoNetworkException
import com.boukharist.leboncoin.utils.extra
import com.boukharist.leboncoin.view.base.ErrorState
import com.boukharist.leboncoin.view.base.LoadingState
import com.boukharist.leboncoin.view.detail.PictureDetailActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.state_view_layout.*
import org.koin.android.architecture.ext.viewModel

class PicturesListActivity : AppCompatActivity() {

    /*
     ************************************************************************************************
     ** Fields
     ************************************************************************************************
     */
    private val viewModel by viewModel<PicturesViewModel>()
    private lateinit var adapter: PicturesAdapter
    private val albumId by extra<Int>(EXTRA_ALBUM_ID)

    /*
     ************************************************************************************************
     ** LifeCycle Methods
     ************************************************************************************************
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pictures_list)
        setupRecycler()
        observeStates()
        viewModel.getPicturesOf(albumId)
    }


    /*
     ************************************************************************************************
     ** Private Methods
     ************************************************************************************************
     */
    private fun observeStates() {
        viewModel.states.observe(this, Observer { state ->
            when (state) {
                is ErrorState -> onDataError(state.error)
                LoadingState -> onDataLoading()
                is PicturesViewModel.LoadedState -> onDataLoaded(state.value)
            }
        })
    }

    private fun onDataLoaded(pics: List<PicEntity>) {
        //hide  state loading
        state_layout.visibility = View.GONE
        //show main content
        recycler.visibility = View.VISIBLE
        //load data
        adapter.setNewData(pics)
    }

    private fun onDataLoading() {
        //hide content
        recycler.visibility = View.GONE
        //show state loading
        state_layout.visibility = View.VISIBLE
        state_content_text_view.setText(R.string.brvah_loading)
        state_image_view.setImageResource(R.drawable.ic_loading)
    }

    private fun onDataError(error: Throwable) {
        @StringRes val message: Int
        @DrawableRes val image: Int

        when (error) {
            is NoNetworkException -> {
                image = R.drawable.ic_network_off
                message = R.string.network_exception_message
            }
            else -> {
                image = R.drawable.ic_error
                message = R.string.unknown_error
            }
        }

        //hide list
        recycler.visibility = View.GONE
        //show state error
        state_layout.visibility = View.VISIBLE
        state_content_text_view.setText(message)
        state_image_view.setImageResource(image)
    }

    private fun setupRecycler() {
        recycler.layoutManager = GridLayoutManager(this, getGridSpanCountByOrientation())
        adapter = PicturesAdapter().apply {
            setHasStableIds(true)
            openLoadAnimation(BaseQuickAdapter.SCALEIN)
            setOnItemClickListener { _, _, position ->
                val pics = this.data
                redirectToPictures(position, pics)
            }
        }

        recycler.adapter = adapter
    }

    private fun redirectToPictures(position: Int, pics: List<PicEntity>) {
        val intent = PictureDetailActivity.newIntent(this, position, pics)
        startActivity(intent)
    }

    private fun getGridSpanCountByOrientation(): Int {
        val orientation = this.resources.configuration.orientation
        return if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            2
        } else {
            4
        }
    }

    companion object {
        private const val EXTRA_ALBUM_ID = "EXTRA_ALBUM_ID"
        fun newIntent(context: Context, albumId: Int): Intent {
            return Intent(context, PicturesListActivity::class.java).apply {
                putExtra(EXTRA_ALBUM_ID, albumId)
            }
        }
    }
}
