package com.boukharist.leboncoin.utils


import android.view.View

import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target


/**
 * Custom [RequestListener] implementation that will hide Loading Animation when actual image is loaded
 */
class GlideLoadingImageListener<R>(private val loadingView: View, private val errorView: View) : RequestListener<R> {

    /*
    ************************************************************************************************
    ** RequestListener<T, R> implementation
    ************************************************************************************************
    */
    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<R>, isFirstResource: Boolean): Boolean {
        hideLoadingIndicator()
        showErrorView()
        return false
    }


    override fun onResourceReady(resource: R, model: Any, target: Target<R>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
        hideLoadingIndicator()
        return false
    }

    /*
    ************************************************************************************************
    ** Private Methods
    ************************************************************************************************
    */
    private fun hideLoadingIndicator() {
        loadingView.visibility = View.GONE
    }

    private fun showErrorView() {
        errorView.visibility = View.VISIBLE
    }
}
