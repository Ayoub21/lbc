package com.boukharist.leboncoin.di

import android.arch.persistence.room.Room
import com.boukharist.leboncoin.data.datasource.local.PicDataBase
import com.boukharist.leboncoin.data.repository.PicsRepository
import com.boukharist.leboncoin.data.repository.PicsRepositoryImpl
import com.boukharist.leboncoin.utils.AppScheduleProvider
import com.boukharist.leboncoin.utils.SchedulerProvider
import com.boukharist.leboncoin.view.main.AlbumViewModel
import com.boukharist.leboncoin.view.pictures.PicturesViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext

val appModule = applicationContext {

    //view models
    viewModel { AlbumViewModel(get(), get()) }
    viewModel { PicturesViewModel(get(), get()) }

    //Repository
    bean { PicsRepositoryImpl(get(), get()) as PicsRepository }

    //misc
    bean { AppScheduleProvider() as SchedulerProvider }
    //DataBase
    bean {
        Room.databaseBuilder(androidApplication(), PicDataBase::class.java, "pic-db")
                .fallbackToDestructiveMigration()
                .build()
    }

    bean { get<PicDataBase>().picturesDao() }

}

val picsApp = listOf(appModule, remoteDatasourceModule)


