package com.boukharist.leboncoin.data.datasource.local

import android.arch.persistence.room.PrimaryKey

data class Album(@PrimaryKey val id: Int,
                 val lastPicId: Int,
                 val picturesCount: Int,
                 val thumbnail: String?)