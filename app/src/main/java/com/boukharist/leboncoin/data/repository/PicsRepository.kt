package com.boukharist.leboncoin.data.repository

import com.boukharist.leboncoin.data.datasource.local.Album
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import io.reactivex.Single

interface PicsRepository {
    fun getAllPics(albumId: Int): Single<List<PicEntity>>
    fun getAllAlbums(): Single<List<Album>>
}