package com.boukharist.leboncoin.data.datasource.local

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.boukharist.leboncoin.data.datasource.remote.PicResponse

@Entity(tableName = "pictures")
data class PicEntity(
        @PrimaryKey val id: Int,
        val albumId: Int,
        val title: String,
        val url: String?,
        val thumbnail: String?)
    : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeInt(albumId)
        writeString(title)
        writeString(url)
        writeString(thumbnail)
    }

    companion object {
        private const val UNKOWN_TITLE = "No Title"

        fun from(source: PicResponse): PicEntity {
            return PicEntity(
                    source.id,
                    source.albumId,
                    source.title ?: UNKOWN_TITLE,
                    source.url,
                    source.thumbnail
            )
        }

        @JvmField
        val CREATOR: Parcelable.Creator<PicEntity> = object : Parcelable.Creator<PicEntity> {
            override fun createFromParcel(source: Parcel): PicEntity = PicEntity(source)
            override fun newArray(size: Int): Array<PicEntity?> = arrayOfNulls(size)
        }
    }
}