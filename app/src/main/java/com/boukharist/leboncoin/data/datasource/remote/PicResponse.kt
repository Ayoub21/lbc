package com.boukharist.leboncoin.data.datasource.remote

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PicResponse(@SerializedName("id") @Expose val id: Int,
                       @SerializedName("albumId") @Expose val albumId: Int,
                       @SerializedName("title") @Expose val title: String?,
                       @SerializedName("url") @Expose val url: String?,
                       @SerializedName("thumbnailUrl") @Expose val thumbnail: String?)