package com.boukharist.leboncoin.data.repository

import com.boukharist.leboncoin.data.datasource.local.Album
import com.boukharist.leboncoin.data.datasource.local.PicDao
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.data.datasource.remote.PicRemoteDataSource
import com.boukharist.leboncoin.data.datasource.remote.PicResponse
import io.reactivex.Completable
import io.reactivex.Single

class PicsRepositoryImpl(private val remoteDataSource: PicRemoteDataSource,
                         private val localDataSource: PicDao) : PicsRepository {


    override fun getAllPics(albumId: Int): Single<List<PicEntity>> {
        return localDataSource.findAllPics(albumId)
                //filter empty source
                .filter { it.isNotEmpty() }
                //if empty fetch and return all albums
                .switchIfEmpty(getPicsFromRemoteAndSave()
                        .andThen(localDataSource.findAllPics(albumId)))
    }

    override fun getAllAlbums(): Single<List<Album>> {
        return localDataSource.findAllAlbums()
                //filter empty source
                .filter { it.isNotEmpty() }
                //if empty fetch and return all albums
                .switchIfEmpty(getPicsFromRemoteAndSave()
                        .andThen(localDataSource.findAllAlbums())
                )
    }

    private fun getPicsFromRemoteAndSave(): Completable {
        return remoteDataSource.getPictures()
                .map { PicConverter()(it) }
                .flatMapCompletable {
                    Completable.fromAction {
                        localDataSource.saveAll(it)
                    }
                }
    }

    private class PicConverter : (List<PicResponse>) -> (List<PicEntity>) {
        override fun invoke(source: List<PicResponse>): List<PicEntity> {
            return source.map { PicEntity.from(it) }
                    .toMutableList()
        }
    }
}