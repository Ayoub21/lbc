package com.boukharist.leboncoin.data.datasource.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [PicEntity::class], version = 1)
abstract class PicDataBase : RoomDatabase() {
    abstract fun picturesDao(): PicDao
}