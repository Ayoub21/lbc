package com.boukharist.leboncoin.data.datasource.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface PicDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(pics: List<PicEntity>)

    @Query("SELECT * FROM pictures where albumId = :albumId ORDER BY id DESC")
    fun findAllPics(albumId: Int): Single<List<PicEntity>>

    @Query("SELECT albumId as id,max(id) as lastPicId,count(*) as picturesCount,thumbnail FROM pictures GROUP BY albumId")
    fun findAllAlbums(): Single<List<Album>>
}