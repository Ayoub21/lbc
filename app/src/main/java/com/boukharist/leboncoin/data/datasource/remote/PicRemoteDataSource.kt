package com.boukharist.leboncoin.data.datasource.remote

import io.reactivex.Single
import retrofit2.http.GET

interface PicRemoteDataSource {
    @GET("/photos")
    fun getPictures(): Single<List<PicResponse>>
}