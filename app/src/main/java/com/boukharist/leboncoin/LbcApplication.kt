package com.boukharist.leboncoin

import android.app.Application
import com.boukharist.leboncoin.di.picsApp
import org.koin.android.ext.android.startKoin

class LbcApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        //start Koin
        startKoin(this, picsApp, extraProperties = mapOf(Pair("SERVER_URL", BuildConfig.SERVER_URL)))
    }
}