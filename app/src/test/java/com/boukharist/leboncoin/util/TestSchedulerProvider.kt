package com.boukharist.leboncoin.util

import com.boukharist.leboncoin.utils.SchedulerProvider
import io.reactivex.schedulers.Schedulers


class TestSchedulerProvider : SchedulerProvider {
    override fun io() = Schedulers.trampoline()

    override fun ui() = Schedulers.trampoline()

    override fun computation() = Schedulers.trampoline()
}