package com.boukharist.leboncoin.util

import com.boukharist.leboncoin.data.datasource.local.Album
import com.boukharist.leboncoin.data.datasource.local.PicEntity
import com.boukharist.leboncoin.data.datasource.remote.PicResponse
import java.util.*

object MockedData {
    private fun getRandomString(): String {
        return UUID.randomUUID().toString()
    }

    val mockedAlbums = listOf(
            Album(1, 30, 30, getRandomString()),
            Album(2, 20, 30, getRandomString()),
            Album(3, 40, 30, getRandomString())
    )

    val picturesResponse = listOf(
            PicResponse(1,1, getRandomString(), getRandomString(), getRandomString()),
            PicResponse(2,1, getRandomString(), getRandomString(), getRandomString()),
            PicResponse(3,1, getRandomString(), getRandomString(), getRandomString()),
            PicResponse(3,2, getRandomString(), getRandomString(), getRandomString())
    )
}