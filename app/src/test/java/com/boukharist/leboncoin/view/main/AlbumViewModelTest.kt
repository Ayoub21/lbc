package com.boukharist.leboncoin.view.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.boukharist.leboncoin.data.repository.PicsRepository
import com.boukharist.leboncoin.util.MockedData
import com.boukharist.leboncoin.util.MockitoHelper.argumentCaptor
import com.boukharist.leboncoin.util.TestSchedulerProvider
import com.boukharist.leboncoin.view.base.ErrorState
import com.boukharist.leboncoin.view.base.LoadingState
import com.boukharist.leboncoin.view.base.ViewModelState
import com.boukharist.leboncoin.view.pictures.PicturesViewModel
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class AlbumViewModelTest {


    private lateinit var viewModel: PicturesViewModel
    @Mock
    private lateinit var statesView: Observer<ViewModelState>
    @Mock
    private lateinit var repository: PicsRepository

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        viewModel = PicturesViewModel(repository, TestSchedulerProvider())
        viewModel.states.observeForever(statesView)
    }

    @Test
    fun `test get albums successfully`() {
        /***************** GIVEN ***************/
        val albums = MockedData.mockedAlbums
        BDDMockito.given(repository.getAllAlbums()).willReturn(Single.just(albums))

        /***************** WHEN ***************/
        viewModel.getAllAlbums()

        /***************** THEN ***************/

        // setup ArgumentCaptor
        val arg = argumentCaptor<ViewModelState>()
        // we expect 2 state changes : [loading,loaded]
        Mockito.verify(statesView, Mockito.times(2)).onChanged(arg.capture())

        val values = arg.allValues
        // Test obtained values in order
        Assert.assertEquals(2, values.size)
        Assert.assertEquals(LoadingState, values[0])
        Assert.assertEquals(PicturesViewModel.LoadedState(albums), values[1])
    }

    @Test
    fun `test get albums failed`() {
        /***************** GIVEN ***************/
        val error = Throwable("Couldn't get albums")
        BDDMockito.given(repository.getAllAlbums()).willReturn(Single.error(error))

        /***************** WHEN ***************/
        viewModel.getAllAlbums()

        /***************** THEN ***************/
        // setup ArgumentCaptor
        val arg = argumentCaptor<ViewModelState>()
        Mockito.verify(statesView, Mockito.times(2)).onChanged(arg.capture())

        val values = arg.allValues
        // Test obtained values in order
        Assert.assertEquals(2, values.size)
        Assert.assertEquals(LoadingState, values[0])
        Assert.assertEquals(ErrorState(error), values[1])
    }

}