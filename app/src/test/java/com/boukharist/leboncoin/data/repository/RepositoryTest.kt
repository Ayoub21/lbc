package com.boukharist.leboncoin.data.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.boukharist.leboncoin.data.datasource.local.PicDao
import com.boukharist.leboncoin.data.datasource.remote.PicRemoteDataSource
import com.boukharist.leboncoin.util.MockedData
import com.boukharist.leboncoin.util.MockitoHelper
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class RepositoryTest : KoinTest {

    private lateinit var repository: PicsRepository

    @Mock
    private lateinit var remoteDataSource: PicRemoteDataSource
    @Mock
    private lateinit var localDataSource: PicDao

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        repository = PicsRepositoryImpl(remoteDataSource, localDataSource)
    }

    @Test
    fun `test get albums success`() {

        //when
        BDDMockito.given(remoteDataSource.getPictures()).willReturn(Single.just(MockedData.picturesResponse))
        BDDMockito.given(localDataSource.findAllAlbums()).willReturn(Single.just(ArrayList()))

        val test = repository.getAllAlbums().test()
        test.awaitTerminalEvent()
        test.assertComplete()

        //check save call
        Mockito.verify(localDataSource).saveAll(MockitoHelper.any())
        Mockito.verify(remoteDataSource).getPictures()
    }

    @Test
    fun `test get albums error`() {
        val error = Throwable("Any Error")
        //when
        BDDMockito.given(remoteDataSource.getPictures()).willReturn(Single.error(error))
        BDDMockito.given(localDataSource.findAllAlbums()).willReturn(Single.error(error))

        val test = repository.getAllAlbums().test()
        test.awaitTerminalEvent()
        test.assertError(error)
    }

}